//
//  ExperienceDetailViewController.swift
//  Personal CV
//
//  Created by Adam Håkansson on 2018-10-30.
//  Copyright © 2018 Adam Håkansson. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    var getTitle = String()
    var getImage = UIImage()
    var getText = String()
    var getDate = String()
    
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var imgImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = getTitle
        labelDate.text = getDate
        imgImage.image = getImage
        labelTitle.text = getTitle
        label1.text = getText
    }
}
