//
//  ExperienceTableViewController.swift
//  Personal CV
//
//  Created by Adam Håkansson on 2018-10-30.
//  Copyright © 2018 Adam Håkansson. All rights reserved.
//

import UIKit

class ExperienceTableViewController: UITableViewController {

    
    
    let job = ["Comunity worker", "postal office"]
    let jobDates = ["2012","2018"]
    let education = ["Fridegårds","JU"]
    let educationDates = ["2014-2017","2017-now"]
    let sections = ["Work experience", "Education"]
    let pictures = ["cleaner","post","school"]
    
    let loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur finibus, nulla et malesuada faucibus, sapien erat euismod lorem, eu pretium neque turpis sed erat. Nam lobortis mi a metus egestas faucibus. Cras aliquam volutpat risus eget efficitur. Ut condimentum nulla sollicitudin faucibus pulvinar. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent vel sollicitudin massa. Morbi ut dignissim est. Mauris non mauris metus. Maecenas et suscipit ipsum. Fusce ex massa, condimentum sed dictum non, tincidunt at neque. Maecenas at finibus sem, sed egestas est. Curabitur at ipsum maximus, scelerisque metus vitae, viverra tellus. Suspendisse vitae ex sed felis laoreet lacinia non quis quam. Donec tempor risus viverra quam finibus gravida. Praesent at massa a nulla tempor porta. Nam ac rutrum arcu, vitae dapibus ante. Donec id augue et mi luctus venenatis ut commodo dolor. Suspendisse aliquam convallis dui id viverra. Ut elementum vestibulum urna, vitae aliquet arcu varius efficitur. Pellentesque vel ultrices arcu, quis rutrum ligula. Nunc suscipit odio risus."
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return job.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ExperienceTableViewCell
        
        switch indexPath.section {
        case 0:
            cell.cellimage.image = UIImage(named: (pictures[indexPath.row]+".png"))
            cell.label1.text = job[indexPath.row]
            cell.label2.text = jobDates[indexPath.row]
            return cell
        case 1:
            cell.cellimage.image = UIImage(named: (pictures[2]+".png"))
            cell.label1.text = education[indexPath.row]
            cell.label2.text = educationDates[indexPath.row]
            return cell
        default:
            return cell
        }
    }
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
 
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let EdvC = Storyboard.instantiateViewController(withIdentifier: "ExperienceDetailViewController") as! ExperienceDetailViewController
        switch indexPath.section {
        case 0:
            EdvC.getImage = UIImage(named: (pictures[indexPath.row]+".png"))!
            EdvC.getTitle = job[indexPath.row]
            EdvC.getDate = jobDates[indexPath.row]
            EdvC.getText = loremIpsum
        case 1:
            EdvC.getImage = UIImage(named: (pictures[2]+".png"))!
            EdvC.getTitle = education[indexPath.row]
            EdvC.getDate = educationDates[indexPath.row]
            EdvC.getText = loremIpsum
        default:
            EdvC.getTitle = "could not get info"
        }
        
        self.navigationController?.pushViewController(EdvC, animated: true)
    }
}
