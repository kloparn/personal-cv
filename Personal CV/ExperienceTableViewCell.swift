//
//  ExperienceTableViewCell.swift
//  Personal CV
//
//  Created by Adam Håkansson on 2018-10-30.
//  Copyright © 2018 Adam Håkansson. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var cellimage: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
