//
//  SkillsViewController.swift
//  Personal CV
//
//  Created by Adam Håkansson on 2018-10-30.
//  Copyright © 2018 Adam Håkansson. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    var loading_1: UIImage!
    var loading_2: UIImage!
    var loading_3: UIImage!
    var images: [UIImage]!
    var animatedImage: UIImage!
    
     @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading_1 = UIImage(named: "loading1")
        loading_2 = UIImage(named: "loading2")
        loading_3 = UIImage(named: "loading3")
        images = [loading_1, loading_2, loading_3]
        animatedImage = UIImage.animatedImage(with: images, duration: 0.75)
        imageView.image = animatedImage
    }
    

   
    

}
